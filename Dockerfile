FROM ruby:2.4.5-stretch

ENV DEBIAN_FRONTEND noninteractive

RUN true \
  && apt-get update \
  && apt-get install -y -qq \
    locales locales-all apt-utils apt-transport-https \
  && rm -rf /var/lib/apt/lists/* \
  && echo "deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main" > /etc/apt/sources.list.d/pgdg.list \
  && echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list \
  && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - \
  && wget --quiet -O - https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
  && apt-get update \
  && curl -sL https://deb.nodesource.com/setup_10.x | bash - 
ENV LANG C
ENV LC_ALL en_GB.UTF-8
RUN true \
  && apt-get install -y -qq --no-install-recommends \
    nodejs yarn \
  && apt-get install -y -qq \
    libpq-dev postgresql-client-10 \
    git curl build-essential libssl-dev apt-utils \
    chromedriver libexif12 \
  && ln -s /usr/lib/chromium/chromedriver /usr/local/bin/chromedriver \
  && rm -rf /var/lib/apt/lists/*
  
RUN true \
  && rm /bin/sh \
  && ln -s /bin/bash /bin/sh \
  && npm install -g sass-lint

# Configure caching
ENV YARN_CACHE_FOLDER /home/chromium/runner-cache/yarn
ENV NPM_CONFIG_CACHE /home/chromium/runner-cache/npm
ENV bower_storage_packages /home/chromium/runner-cache/bower
ENV GEM_SPEC_CACHE /home/chromium/runner-cache/gem

ENTRYPOINT [ "/bin/bash", "-c" ]
CMD [ "bash" ]
RUN true \
  && echo "gem: --no-document" > /etc/gemrc \
  && gem install bundler
